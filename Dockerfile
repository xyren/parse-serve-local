FROM node:latest

RUN mkdir parse

ADD . /parse
WORKDIR /parse
RUN npm install

# Should end with slash
ENV MONGODB_URI 'mongodb://localhost:27017/'

ENV APP_NAME devApp
ENV APP_ID myAppId
ENV MASTER_KEY masterKey

# URL as serve
ENV SERVER_URL 'http://localhost'
ENV SERVER_PORT 1337

# Dashboard access
ENV DASHBOARD_USER admin
ENV DASHBOARD_PW admin

EXPOSE SERVER_PORT

# Uncomment if you want to access cloud code outside of your container
# A main.js file must be present, if not Parse will not start

CMD [ "npm", "start" ]
